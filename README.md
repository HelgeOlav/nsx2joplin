# Introduction

This is a quick home-made tool to convert the Synology DS Note export format ino the sync format used by Joplin. The idea is to be able for me to convert my existing DS Note database into Joplin.

This tool does not support to-dos but will handle attachments and will create new tags and notebooks for everything you had. It will not merge with an existing Joplin database (meaning existing tags and notebooks will be duplicated).

The content from DS Note is converted to MarkDown using the github.com/JohannesKaufmann/html-to-markdown package.

# Install

Clone or download the source files in this directory. Build it using "go build". There is only one external dependency in this code.

```
go get github.com/JohannesKaufmann/html-to-markdown
go get bitbucket.org/HelgeOlav/nsx2joplin
```

# How to use

Unzip the NSX export file into an empty directory.

```bash
nsx2joplin -src=../nsx -dst=../joplin-sync-dir
```