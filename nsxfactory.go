package main

import (
	"encoding/json"
	"fmt"
	md "github.com/JohannesKaufmann/html-to-markdown"
	"io/ioutil"
	"os"
	"time"
)

// The factory loads the configuration file

type NsxConfig struct {
	Notebooks []string `json:"notebook"`
	Notes     []string `json:"note"`
}

type NsxNotebook struct {
	Category string `json:"category"`
	Name     string `json:"title"`
	Ctime    int64  `json:"ctime"`
	Utime    int64  `json:"mtime"`
	Stack    string `json:"stack"`
}

type NsxNote struct {
	Brief      string             `json:"brief"`
	Body       string             `json:"content"`
	Ctime      int64              `json:"ctime"`
	Utime      int64              `json:"mtime"`
	Book       string             `json:"parent_id"`
	Tag        []string           `json:"tag"`
	Title      string             `json:"title"`
	Location   string             `json:"location"`
	URL        string             `json:"source_url"`
	Latitude   float64            `json:"latitude"`
	Longitude  float64            `json:"longitude"`
	Attachment map[string]NsxFile `json:"attachment"`
}

type NsxFile struct {
	Ext  string `json:"ext"`
	MD5  string `json:"md5"`
	Name string `json:"name"`
	size int    `json:"size"`
}

func NewNSXFactory() *NsxConfig {
	filename := nsxpath + "/config.json"
	jsonFile, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer jsonFile.Close()
	// try parse the file
	byteValue, _ := ioutil.ReadAll(jsonFile)
	var config NsxConfig
	err = json.Unmarshal(byteValue, &config)
	if err != nil {
		panic(err)
	}
	return &config
}

// Create notebooks from config
func CreateNotebooksFromNSX(config *NsxConfig) {
	for _, val := range config.Notebooks {
		// check if file exist
		fileName := nsxpath + "/" + val
		if !fileExists(fileName) {
			fmt.Printf("Notebook import failed, file does not exist: %s\n", val)
			return
		}
		// load and parse json
		jsonFile, err := os.Open(fileName)
		if err != nil {
			fmt.Printf("Failed to open notebook %s\n", fileName)
			return
		}
		byteValue, _ := ioutil.ReadAll(jsonFile)
		jsonFile.Close()
		var nbinfo NsxNotebook
		err = json.Unmarshal(byteValue, &nbinfo)
		if err != nil {
			continue
		}
		// create notebook if it has a name
		if len(nbinfo.Name) == 0 {
			nbinfo.Name = "Unnamed book"
		}
		nb := NewNotebook(nbinfo.Name)
		nb.sourceID = val
		nb.ctime = time.Unix(nbinfo.Ctime, 0)
		nb.utime = time.Unix(nbinfo.Utime, 0)
		nb.Write()
		AddNotebook(nb)
	}
}

//func parseUnixDate(input string) time.Time {
//	if len(input) > 0 {
//		num, err := strconv.ParseInt(input, 10, 0)
//		if err != nil {
//			return time.Unix(num, 0)
//		}
//	}
//	return time.Now()
//}

type ConvertNote func(noteid string) bool

func CreateNotesFromNSX(config *NsxConfig, convertnote ConvertNote) {
	// loop through all notes
	var numProsessed = 0
	var numSkipped = 0

	for _, noteid := range config.Notes {
		// check if note will be converted
		if convertnote(noteid) == false {
			numSkipped++
			continue
		}
		numProsessed++
		// check if file exist
		fileName := nsxpath + "/" + noteid
		if !fileExists(fileName) {
			fmt.Printf("Note import failed, file does not exist: %s\n", noteid)
			continue
		}
		// load and parse json
		jsonFile, err := os.Open(fileName)
		if err != nil {
			fmt.Printf("Failed to open notebook %s\n", fileName)
			continue
		}
		byteValue, _ := ioutil.ReadAll(jsonFile)
		jsonFile.Close()
		var nbinfo NsxNote
		err = json.Unmarshal(byteValue, &nbinfo)
		if err != nil {
			fmt.Println(err)
		}
		// import note
		if len(nbinfo.Body) > 0 {
			fmt.Printf("Importing note %s - %s\n", nbinfo.Title, noteid)
			note := NewNote(nbinfo.Title)
			note.ctime = time.Unix(nbinfo.Ctime, 0)
			note.utime = time.Unix(nbinfo.Utime, 0)
			note.Book = GetNotebookIDBySourceID(nbinfo.Book)
			// convert from HTML to MD on the body
			converter := md.NewConverter("", true, nil)
			md, err := converter.ConvertString(nbinfo.Body)
			if err == nil {
				note.Body = md
			} else {
				note.Body = nbinfo.Body
			}

			note.SourceID = noteid
			// handle tags
			if len(nbinfo.Tag) > 0 {
				var newTags []Tag
				for _, tag := range nbinfo.Tag {
					newTags = append(newTags, GetTagByName(tag))
				}
				note.Tags = &newTags
				note.WriteTags()
			}
			// handle other metadata
			if len(nbinfo.URL) > 0 {
				note.Metadata["source_url"] = nbinfo.URL
			}
			if nbinfo.Latitude != 0 {
				note.Metadata["latitude"] = fmt.Sprintf("%f", nbinfo.Latitude)
				note.Metadata["longitude"] = fmt.Sprintf("%f", nbinfo.Longitude)
			}
			note.Metadata["source_application"] = "nsx2joplin"
			// handle attachments
			for _, v := range nbinfo.Attachment {
				attFile := nsxpath + "/file_" + v.MD5
				att, err := NewAttachment(attFile)
				if err != nil {
					fmt.Println(err)
					continue
				}
				// add link to body of message.
				note.Body = note.Body + fmt.Sprintf("\n\n[%s](:/%s)\n", v.Name, att.ID)
				att.Write()
			}
			// strip HTML tags from documents
			//note.Body = strip.StripTags(note.Body)
			// done
			note.Write()
		} // note to import
	} // for loop
	fmt.Printf("Notes skipped: %d\n", numSkipped)
	fmt.Printf("Notes processed: %d\n", numProsessed)
}
