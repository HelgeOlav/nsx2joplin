package main

import (
	"fmt"
	"os"
	"time"
)

type Note struct {
	Name     string
	Body     string
	Book     *Notebook // pointer to the notebook
	Metadata map[string]string
	Tags     *[]Tag
	ID       string
	SourceID string
	ctime    time.Time // created time
	utime    time.Time // updated time
}

func (t *Note) UpdateTimeStamps() {
	if t != nil {
		t.utime = time.Now()
		if t.ctime.IsZero() {
			t.ctime = t.utime
		}
	}
}

func NewNote(name string) Note {
	t := new(Note)
	t.Name = name
	t.Metadata = make(map[string]string)
	t.Metadata["source_applicaton"] = "nsx2joplin"
	t.Metadata["author"] = "nsx2joplin"
	t.ID = MakeID()
	t.UpdateTimeStamps()
	return *t
}

/* all attributes
Testnotat

Jeg tester  med et notat.

id: ce5e881f1b3c4a1181751565ff18dd86
parent_id: 68c73c1d5e5c45468fabde5f358dc1d1
created_time: 2019-12-23T15:43:09.283Z
updated_time: 2019-12-23T15:51:06.819Z
is_conflict: 0
latitude: 44.94470000
longitude: 20.21970000
altitude: 0.0000
author:
source_url:
is_todo: 0
todo_due: 0
todo_completed: 0
source: joplin-desktop
source_application: net.cozic.joplin-desktop
application_data:
order: 0
user_created_time: 2019-12-23T15:43:00.000Z
user_updated_time: 2019-12-23T15:51:06.819Z
encryption_cipher_text:
encryption_applied: 0
markup_language: 1
type_: 1
*/

/* shortest possible document
subj

body

id: 09cd41b5ade54f6db3110fc6b18519ff
parent_id: 68c73c1d5e5c45468fabde5f358dc1d1
created_time: 2019-12-23T15:57:40.100Z
updated_time: 2019-12-23T15:59:15.365Z
markup_language: 1
type_: 1
*/

func (n *Note) Write() {
	filename := syncpath + "/" + n.ID + ".md"
	f, err := os.Create(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	f.WriteString(n.Name)
	fmt.Fprintf(f, "\n\n%s\n\nid: %s\nparent_id: %s\ncreated_time: %s\nupdated_time: %s\n", n.Body, n.ID, n.Book.ID, n.ctime.Format(time.RFC3339), n.utime.Format(time.RFC3339))
	// write metadata, if any
	if n.Metadata != nil {
		for k, v := range n.Metadata {
			fmt.Fprintf(f, "%s: %s\n", k, v)
		}
	}
	f.WriteString("markup_language: 1\ntype_: 1")
}
