package main

import (
	"errors"
	"fmt"
	"io"
	"mime"
	"net/http"
	"os"
	"path"
	"time"
)

type Attachment struct {
	ID         string
	Metadata   map[string]string
	sourceFile string
	//mime string
	//ctime time.Time // created time
	//utime time.Time // updated time
	size int64
	//ext string
}

// source: https://golangcode.com/check-if-a-file-exists/
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func NewAttachment(filename string) (*Attachment, error) {
	att := new(Attachment)
	att.ID = MakeID()
	att.Metadata = make(map[string]string)
	att.sourceFile = filename
	att.UpdateTimeStamps()
	_, fn := path.Split(filename)
	att.Metadata["filename"] = fn
	// verify source file exists
	if fileExists(filename) == false {
		return nil, errors.New("File does not exist")
	}
	// get source metadata
	fi, err := os.Stat(filename)
	if err != nil {
		return nil, err
	}
	// size
	att.size = fi.Size()
	ext, mimes := identifyFile(filename)
	// determine MIME type
	// mimes := mime.TypeByExtension(path.Ext(filename))
	if len(mimes) > 0 {
		att.Metadata["mime"] = mimes
	}
	if len(ext) > 0 {
		att.Metadata["file_extension"] = ext
	}
	return att, nil
}

func (t *Attachment) UpdateTimeStamps() {
	//if t != nil {
	//	t.utime = time.Now()
	//	if t.ctime.IsZero() {
	//		t.ctime = t.utime
	//	}
	//}
	if t != nil {
		t.Metadata["created_time"] = time.Now().Format(time.RFC3339)
		t.Metadata["updated_time"] = t.Metadata["created_time"]
	}
}

func (att *Attachment) Write() error {
	// first write the metadata file
	filename := syncpath + "/" + att.ID + ".md"
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	fmt.Fprintf(f, "id: %s\n", att.ID)
	for k, v := range att.Metadata {
		fmt.Fprintf(f, "%s: %s\n", k, v)
	}
	fmt.Fprintf(f, "size: %d\ntype_: 4", att.size)
	// copy file
	destFile := syncpath + "/.resource/" + att.ID
	return copyFile(att.sourceFile, destFile)
	// return nil
}

/*
acoustic-guitar-57e4d3414c_1920.png

id: 0a2eb0b8d6ae435985e3c58ad9ebbd88
mime: image/png
filename:
created_time: 2019-12-23T17:44:37.766Z
updated_time: 2019-12-23T17:44:37.766Z
user_created_time: 2019-12-23T17:44:37.766Z
user_updated_time: 2019-12-23T17:44:37.766Z
file_extension: png
encryption_cipher_text:
encryption_applied: 0
encryption_blob_encrypted: 0
size: 336816
type_: 4
*/

func copyFile(src, dst string) error {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer destination.Close()
	_, err = io.Copy(destination, source)
	return err
}

func identifyFile(filename string) (ext, contentType string) {
	fi, err := os.Stat(filename)
	if err != nil {
		fmt.Println(err)
		return "", ""
	}
	// check if directory
	if fi.IsDir() {
		return "", ""
	}
	// load 512 bytes of the file
	f, err := os.Open(filename)
	if err != nil {
		fmt.Println(err)
		return "", ""
	}
	defer f.Close()
	buffer := make([]byte, 512)
	_, err = f.Read(buffer)
	if err != nil {
		fmt.Println(err)
		return "", ""
	}
	// now try to detect mime type
	contentType = http.DetectContentType(buffer)
	// and get an extension
	exts, _ := mime.ExtensionsByType(contentType)
	if len(exts) > 0 {
		ext = exts[0][1:]
	}
	return ext, contentType
}
