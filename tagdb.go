package main

import "strings"

var myTags []Tag

// return tag, create if it does not exist

func GetTagByName(name string) Tag {
	// see if the tag is in the database
	for _, t := range myTags {
		result := strings.Compare(t.Name, name)
		if result == 0 {
			return t
		} // result == 0
	} // for
	// create new tag
	tag := NewTag(name)
	tag.Write()
	myTags = append(myTags, tag)
	return tag
} // func
