package main

// Contains a list of all Notebooks and helper functions

var MyNotebooks []Notebook

// Add a new notebook to the list of notebooks
func AddNotebook(n Notebook) {
	MyNotebooks = append(MyNotebooks, n)
}

func GetNotebookIDBySourceID(ID string) *Notebook {
	for _, nb := range MyNotebooks {
		if nb.sourceID == ID {
			return &nb
		}
	}
	return nil
}
