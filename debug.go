package main

import "fmt"

// returns object for existing notebook

func GetNotebook() Notebook {
	n := new(Notebook)
	n.ID = "68c73c1d5e5c45468fabde5f358dc1d1"
	n.UpdateTimeStamps()
	n.Name = "Notatblokk 1"
	return *n
}

func GetTagg1() Tag {
	t := new(Tag)
	t.ID = "cefd525c483046b6ac4a7288696f4a59"
	t.Name = "Tagg 1"
	t.UpdateTimeStamps()
	return *t
}

func WriteTestNote() {
	tagg1 := GetTagg1()
	tagg2 := NewTag("tagg 2")
	tagg2.Write()
	notebook := GetNotebook()
	note := NewNote("testdokument 1")
	note.Body = "Helge var her"
	note.Book = &notebook
	note.Tags = &[]Tag{tagg1, tagg2}
	note.Metadata["source_url"] = "http://www.vg.no"
	note.WriteTags()
	note.Write()
}

func testAttachment() {
	att, err := NewAttachment("/home/bruker/Downloads/piano-55e1d6464d_1920.png")
	fmt.Println(*att)
	fmt.Println(err)
	fmt.Println(att.Write())
}
