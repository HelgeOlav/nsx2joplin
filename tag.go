package main

import (
	"os"
	"time"
)

type Tag struct {
	Name  string
	ID    string
	ctime time.Time // created time
	utime time.Time // updated time
}

func NewTag(name string) Tag {
	t := new(Tag)
	t.Name = name
	t.ID = MakeID()
	t.UpdateTimeStamps()
	return *t
}

// Updates the updated time, and also sets created time if not present
func (t *Tag) UpdateTimeStamps() {
	if t != nil {
		t.utime = time.Now()
		if t.ctime.IsZero() {
			t.ctime = t.utime
		}
	}
}

// write tag to file
func (t *Tag) Write() {
	filename := syncpath + "/" + t.ID + ".md"
	f, err := os.Create(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	f.WriteString(t.Name)
	f.WriteString("\n\nid: ")
	f.WriteString(t.ID)
	//f.WriteString("\ncreated_time: 2019-08-24T20:32:24.037Z\nupdated_time: 2019-08-24T20:32:24.037Z\nuser_created_time: 2019-08-24T20:32:24.037Z\nuser_updated_time: 2019-08-24T20:32:24.037Z\n")
	f.WriteString("\ncreated_time: ")
	f.WriteString(t.ctime.Format(time.RFC3339))
	f.WriteString("\nupdated_time: ")
	f.WriteString(t.utime.Format(time.RFC3339))
	f.WriteString("\nuser_created_time: ")
	f.WriteString(t.ctime.Format(time.RFC3339))
	f.WriteString("\nuser_updated_time: ")
	f.WriteString(t.utime.Format(time.RFC3339))
	f.WriteString("\ntype_: 5")
	// fmt.Println(t.Format(time.RFC3339))
}
