package main

import (
	"os"
	"time"
)

type Notebook struct {
	Name     string
	ID       string
	ctime    time.Time // created time
	utime    time.Time // updated time
	sourceID string    // ID from source system
}

func (t *Notebook) UpdateTimeStamps() {
	if t != nil {
		t.utime = time.Now()
		if t.ctime.IsZero() {
			t.ctime = t.utime
		}
	}
}

func NewNotebook(name string) Notebook {
	t := new(Notebook)
	t.Name = name
	t.ID = MakeID()
	t.UpdateTimeStamps()
	return *t
}

// write tag to file
func (t *Notebook) Write() {
	filename := syncpath + "/" + t.ID + ".md"
	f, err := os.Create(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	f.WriteString(t.Name)
	f.WriteString("\n\nid: ")
	f.WriteString(t.ID)
	f.WriteString("\ncreated_time: ")
	f.WriteString(t.ctime.Format(time.RFC3339))
	f.WriteString("\nupdated_time: ")
	f.WriteString(t.utime.Format(time.RFC3339))
	f.WriteString("\nuser_created_time: ")
	f.WriteString(t.ctime.Format(time.RFC3339))
	f.WriteString("\nuser_updated_time: ")
	f.WriteString(t.utime.Format(time.RFC3339))
	f.WriteString("encryption_cipher_text:\nencryption_applied: 0\nparent_id:\n")
	f.WriteString("type_: 2")
	// fmt.Println(t.Format(time.RFC3339))
}
