package main

import (
	"fmt"
	"os"
	"time"
)

// support file for writing out tags to notes

type NoteTag struct {
	ID      string
	note_id string
	tag_id  string
	ctime   time.Time
	utime   time.Time
}

func (t *NoteTag) UpdateTimeStamps() {
	if t != nil {
		t.utime = time.Now()
		if t.ctime.IsZero() {
			t.ctime = t.utime
		}
	}
}

func (t *NoteTag) Write() error {
	if t != nil {
		filename := syncpath + "/" + t.ID + ".md"
		f, err := os.Create(filename)
		if err != nil {
			return err
		}
		defer f.Close()
		_, err = fmt.Fprintf(f, "id: %s\nnote_id: %s\ntag_id: %s\ncreated_time: %s\nupdated_time: %s\ntype_: 6",
			t.ID,
			t.note_id,
			t.tag_id,
			t.ctime.Format(time.RFC3339),
			t.utime.Format(time.RFC3339))
		return err
	}
	return nil
}

func NewNoteTag(n *Note, tag *Tag) NoteTag {
	t := new(NoteTag)
	t.ID = MakeID()
	t.UpdateTimeStamps()
	t.tag_id = tag.ID
	t.note_id = n.ID
	return *t
}

func (n *Note) WriteTags() error {
	if n != nil {
		if n.Tags != nil {
			for _, tag := range *n.Tags {
				nt := NewNoteTag(n, &tag)
				nt.Write()
			}
		}
	}
	return nil
}

/* Format for note-to-tag document
id: 1e8d1268c8d34818991ee835b0761ded
note_id: d4060d498fc64fd28df82c9b4e5eaf62
tag_id: cefd525c483046b6ac4a7288696f4a59
created_time: 2019-08-24T20:32:24.076Z
updated_time: 2019-08-24T20:32:24.076Z
user_created_time: 2019-08-24T20:32:24.076Z
user_updated_time: 2019-08-24T20:32:24.076Z
encryption_cipher_text:
encryption_applied: 0
type_: 6
*/
