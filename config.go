package main

import "flag"

var syncpath string
var nsxpath string

func parseCmd() {
	flag.StringVar(&syncpath, "dst", "/Users/helge/temp/joplin/sync", "Destination")
	flag.StringVar(&nsxpath, "src", "/Users/helge/temp/joplin/nsx", "Source")
	flag.Parse()
}
