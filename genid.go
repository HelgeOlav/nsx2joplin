package main

import (
	"math/rand"
	"time"
)

// from https://www.calhoun.io/creating-random-strings-in-go/
var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func MakeID() string {
	// return StringWithCharset(32, "abcdef0123456789")
	return "ffff" + StringWithCharset(28, "abcdef0123456789")
}
